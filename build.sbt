import sbt.Keys.libraryDependencies
import sbt._
import Dependencies.deps

lazy val server = (project in file("server"))
  .enablePlugins(UniversalPlugin)
  .enablePlugins(AshScriptPlugin)
  .enablePlugins(DockerPlugin)
  .settings(
    Settings.common
  )
  .settings(
    packageName in Docker := "re-iot-swimming-pool",
    dockerBaseImage := "openjdk:16-jdk",
    dockerExposedPorts := Seq(8081),
    version in Docker := "1.0.1",
    dockerUsername := Some("ivoya"),
    dockerUpdateLatest := true
  )
  .settings(
    libraryDependencies ++= deps.core,
    libraryDependencies ++= deps.derevo,
    libraryDependencies ++= deps.config
  )
  .settings(
    libraryDependencies ++= Dependencies.`zio-test`,
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")
  )

