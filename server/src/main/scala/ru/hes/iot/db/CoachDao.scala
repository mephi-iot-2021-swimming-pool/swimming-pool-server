package ru.hes.iot.db

import org.mongodb.scala.MongoClient
import ru.hes.iot.db.impl.mongo.CoachDaoMongoImpl
import ru.hes.iot.db.impl.mongo.config.MongoDaoConfig
import ru.hes.iot.domain.Coach
import zio.{Has, Task, ZLayer}

import java.util.UUID


trait CoachDao[F[_]] {
  def getCoach(coachId: UUID): F[Option[Coach]]

  def insertCoach(coach: Coach): F[Unit]

  def deleteCoach(coachId: UUID): F[Unit]

  def updateCoach(coachId: UUID, newCoach: Coach): F[Unit]
}

object CoachDao {
  val live: ZLayer[Has[MongoDaoConfig] with Has[MongoClient], Nothing, Has[CoachDao[Task]]] =
    (CoachDaoMongoImpl(_, _)).toLayer
}
