package ru.hes.iot.db.impl.mongo.config

case class MongoDaoConfig(databaseName: String, collectionName: String)
