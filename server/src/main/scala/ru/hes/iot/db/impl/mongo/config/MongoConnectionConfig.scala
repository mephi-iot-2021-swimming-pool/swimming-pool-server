package ru.hes.iot.db.impl.mongo.config

import com.typesafe.config.ConfigFactory
import derevo.derive
import derevo.pureconfig.{pureconfigReader, pureconfigWriter}
import pureconfig.ConfigSource
import zio.{Has, Task, TaskLayer, ZLayer}

@derive(pureconfigReader, pureconfigWriter)
case class MongoConnectionConfig(url: String)

object MongoConnectionConfig {
  val live: TaskLayer[Has[MongoConnectionConfig]] = ZLayer.fromEffect {
    Task
      .fromEither(
        ConfigSource
          .fromConfig(ConfigFactory.defaultReference().getConfig("mongo"))
          .load[MongoConnectionConfig]
          .left
          .map(pureconfig.error.ConfigReaderException.apply)
      )
  }
}
