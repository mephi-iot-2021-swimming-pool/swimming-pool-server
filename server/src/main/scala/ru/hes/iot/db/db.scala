package ru.hes.iot

import com.mongodb.MongoClientSettings
import org.mongodb.scala.{ConnectionString, MongoClient}
import ru.hes.iot.db.impl.mongo.config.MongoConnectionConfig
import zio.{Has, Task, ZIO, ZLayer, ZManaged}

import scala.util.Try

package object db {
  object DBConnectionLive {

    private val managedMongoClient: ZManaged[Has[MongoConnectionConfig], Throwable, MongoClient] =
      for {
        endpoint    <- ZIO.access[Has[MongoConnectionConfig]](_.get.url).toManaged_
        mongoClient <- ZManaged.make(acquireConnection(endpoint))(releaseConnection)
      } yield mongoClient

    private def acquireConnection(databaseUri: String): Task[MongoClient] =
      ZIO.fromTry(Try {
        val clientSettings = MongoClientSettings.builder
          .applyConnectionString(new ConnectionString(databaseUri))
          .build

        MongoClient(clientSettings)
      })

    private def releaseConnection(mongoClient: MongoClient): ZIO[Any, Nothing, Unit] =
      ZIO.fromTry(Try(mongoClient.close)).orElse(ZIO.succeed(()))

    val layer: ZLayer[Has[MongoConnectionConfig], Throwable, Has[MongoClient]] =
      ZLayer.fromManaged(managedMongoClient)
  }
}
