package ru.hes.iot.db.impl.mongo

import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.{CodecProvider, CodecRegistry}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.bson.codecs.Macros
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.Projections.excludeId
import org.mongodb.scala.{MongoClient, MongoCollection}
import ru.hes.iot.db.CoachDao
import ru.hes.iot.db.impl.mongo.CoachDaoMongoImpl.codecRegistry
import ru.hes.iot.db.impl.mongo.config.MongoDaoConfig
import ru.hes.iot.domain.{Athlete, Coach, Exercise, Programme}
import zio.{Task, ZIO}

import java.util.UUID
import scala.concurrent.ExecutionContext
import scala.reflect.ClassTag
import scala.util.Try

class CoachDaoMongoImpl private (
    config: MongoDaoConfig,
    mongoClient: MongoClient
) extends CoachDao[Task] {

  override def getCoach(coachId: UUID): Task[Option[Coach]] = withCollection[Option[Coach], Coach] {
    collection =>
      Task
        .fromFuture(_ =>
          collection.find(equal("_id", coachId.toString)).head()
        )
        .map(Option(_))
  }

  override def insertCoach(coach: Coach): Task[Unit] = withCollection[Unit, Coach] { collection =>
    Task.fromFuture((ec: ExecutionContext) => collection.insertOne(coach).head().map(_ => ())(ec))
  }

  override def deleteCoach(coachId: UUID): Task[Unit] = withCollection[Unit, Coach] { collection =>
    Task.fromFuture((ec: ExecutionContext) =>
      collection.deleteOne(equal("_id", coachId.toString)).head().map(_ => ())(ec)
    )
  }

  override def updateCoach(coachId: UUID, newCoach: Coach): Task[Unit] =
    withCollection[Unit, Coach] { collection =>
      Task.fromFuture((ec: ExecutionContext) =>
        collection
          .findOneAndReplace(equal("_id", coachId.toString), newCoach)
          .head()
          .map(_ => ())(ec)
      )

    }

  private def withCollection[A, T : ClassTag](
      f: MongoCollection[T] => Task[A]
  ): Task[A] = {
    val collectionZIO = ZIO.fromTry(Try {
      mongoClient
        .getDatabase(config.databaseName)
        .withCodecRegistry(codecRegistry)
        .getCollection[T](config.collectionName)
    })

    collectionZIO.flatMap(f)
  }
}

object CoachDaoMongoImpl {
  def apply(config: MongoDaoConfig, mongoClient: MongoClient) =
    new CoachDaoMongoImpl(config, mongoClient)

  private val coachCodecProvider: CodecProvider = Macros.createCodecProviderIgnoreNone[Coach]()
  private val athleteCodecProvider: CodecProvider = Macros.createCodecProviderIgnoreNone[Athlete]()
  private val programmeCodecProvider: CodecProvider =
    Macros.createCodecProviderIgnoreNone[Programme]()
  private val exerciseCodecProvider: CodecProvider =
    Macros.createCodecProviderIgnoreNone[Exercise]()

  private val codecRegistry: CodecRegistry = fromRegistries(
    fromProviders(
      coachCodecProvider,
      athleteCodecProvider,
      programmeCodecProvider,
      exerciseCodecProvider
    ),
    DEFAULT_CODEC_REGISTRY
  )
}
