package ru.hes.iot.domain

import derevo.circe.{decoder, encoder}
import derevo.derive

object Errors {
  @derive(encoder, decoder)
  case class AthleteNotFound(msg: String) extends Throwable(msg)
  @derive(encoder, decoder)
  case class CoachNotFound(msg: String) extends Throwable(msg)
}
