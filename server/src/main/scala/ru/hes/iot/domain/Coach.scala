package ru.hes.iot.domain

import derevo.circe.{decoder, encoder}
import derevo.derive


@derive(encoder, decoder)
case class Coach(
    _id: String,
    login: String,
    name: String,
    password: String,
    athletes: Seq[Athlete],
    programmes: Seq[Programme]
)

@derive(encoder, decoder)
case class CoachDTO(login: String,
                    name: String,
                    password: String,
                    athletes: Seq[Athlete],
                    programmes: Seq[Programme])

