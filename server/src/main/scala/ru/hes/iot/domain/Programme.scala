package ru.hes.iot.domain

import derevo.circe.{decoder, encoder}
import derevo.derive

@derive(encoder, decoder)
case class Programme(name: String, exercises: Map[String, Exercise])

@derive(encoder, decoder)
case class Exercise(duration: Int, unit: String, lower: Int, upper: Int)
