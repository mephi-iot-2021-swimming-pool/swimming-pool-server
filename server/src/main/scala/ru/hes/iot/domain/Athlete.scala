package ru.hes.iot.domain

import derevo.circe.{decoder, encoder}
import derevo.derive

@derive(encoder, decoder)
case class Athlete(name: String, history: Map[String, String])
