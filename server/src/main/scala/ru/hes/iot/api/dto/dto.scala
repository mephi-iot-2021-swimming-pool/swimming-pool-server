package ru.hes.iot.api.dto

import derevo.circe.{decoder, encoder}
import derevo.derive
import ru.hes.iot.domain.{Coach, CoachDTO, Programme}

import java.util.UUID


object dto {
  @derive(encoder, decoder)
  case class GetHistoryByAthleteRequest(coachId: UUID, athleteName: String)
  @derive(encoder, decoder)
  case class UpdateHistoryRequest(
      coachId: UUID,
      athleteName: String,
      newTrainings: Map[String, String]
  )
  @derive(encoder, decoder)
  case class GetProgrammesRequest(coachId: UUID)
  @derive(encoder, decoder)
  case class AddProgrammeRequest(
      coachId: UUID,
      programme: Programme
  )
  @derive(encoder, decoder)
  case class DeleteProgrammeRequest(
      coachId: UUID,
      programme: Programme
  )
  @derive(encoder, decoder)
  case class DeleteCoachRequest(coachId: UUID)
  @derive(encoder, decoder)
  case class AddCoachRequest(coachDTO: CoachDTO)
  @derive(encoder, decoder)
  case class GetCoachByIdRequest(coachId: UUID)

  @derive(encoder, decoder)
  case class AddCoachResponse(coachId: UUID)
  @derive(encoder, decoder)
  case class GetCoachByIdResponse(coach: Coach)
}
