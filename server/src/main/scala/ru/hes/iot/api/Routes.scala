package ru.hes.iot.api

import derevo.circe.{decoder, encoder}
import derevo.derive
import io.circe.generic.decoding.DerivedDecoder.deriveDecoder
import sttp.model.StatusCode
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import sttp.tapir.openapi.circe.yaml.RichOpenAPI
import sttp.tapir.server.http4s.ztapir.ZHttp4sServerInterpreter
import sttp.tapir.swagger.SwaggerUI
import sttp.tapir.ztapir._
import zio.{RIO, _}
import zio.blocking.Blocking
import zio.clock.Clock
import zio.interop.catz._
import org.http4s.HttpRoutes
import ru.hes.iot.api.dto.dto._
import ru.hes.iot.domain.{Athlete, Coach, Programme}
import ru.hes.iot.domain.Errors.{AthleteNotFound, CoachNotFound}
import ru.hes.iot.service.CoachService

import java.util.UUID

object Routes {

  @derive(encoder, decoder)
  case class GeneralFail(detailMessage: String) extends Throwable(detailMessage)

  val addCoach: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("addCoach")
      .in(jsonBody[AddCoachRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .out(jsonBody[AddCoachResponse])
      .zServerLogic(req => {

        val _id = UUID.randomUUID()
        val newCoach = Coach(
          _id.toString,
          req.coachDTO.login,
          req.coachDTO.name,
          req.coachDTO.password,
          req.coachDTO.athletes,
          req.coachDTO.programmes
        )
        CoachService.addCoach(newCoach).map(_ => AddCoachResponse(_id))
      })

  val deleteCoach: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("deleteCoach")
      .in(jsonBody[DeleteCoachRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .zServerLogic(deleteReq => CoachService.deleteCoach(deleteReq.coachId))

  val getCoachById: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("getCoachById")
      .in(jsonBody[GetCoachByIdRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[CoachNotFound].description("Coach with given Id was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .out(jsonBody[Coach])
      .zServerLogic(coachReq => CoachService.getCoachById(coachReq.coachId))

  val getHistoryByAthlete: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("getHistoryByAthlete")
      .in(jsonBody[GetHistoryByAthleteRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[AthleteNotFound].description("Athlete with given Name was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[CoachNotFound].description("Coach with given Id was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .out(jsonBody[Map[String, String]])
      .zServerLogic(historyReq =>
        CoachService.getHistoryByAthlete(historyReq.coachId, historyReq.athleteName)
      )

  val updateHistory: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("updateHistory")
      .in(jsonBody[UpdateHistoryRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[AthleteNotFound].description("Athlete with given Name was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[CoachNotFound].description("Coach with given Id was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .zServerLogic(updateHistoryReq =>
        CoachService.updateHistory(
          updateHistoryReq.coachId,
          updateHistoryReq.athleteName,
          updateHistoryReq.newTrainings
        )
      )

  val getProgrammes: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("getProgrammes")
      .in(jsonBody[GetProgrammesRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[CoachNotFound].description("Coach with given Id was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .out(jsonBody[Seq[Programme]])
      .zServerLogic(programmesReq => CoachService.getProgrammes(programmesReq.coachId))

  val addProgramme: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("addProgramme")
      .in(jsonBody[AddProgrammeRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[CoachNotFound].description("Coach with given Id was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .zServerLogic(addProgrammeReq =>
        CoachService.addProgramme(addProgrammeReq.coachId, addProgrammeReq.programme)
      )

  val deleteProgramme: ZServerEndpoint[Has[CoachService[Task]], Any] =
    endpoint.post
      .in("deleteProgramme")
      .in(jsonBody[DeleteProgrammeRequest])
      .errorOut(
        oneOf[Throwable](
          oneOfVariantFromMatchType(
            StatusCode.BadRequest,
            jsonBody[CoachNotFound].description("Coach with given Id was not found")
          ),
          oneOfVariantFromMatchType(
            StatusCode.InternalServerError,
            jsonBody[GeneralFail].description("Something went wrong")
          )
        )
      )
      .zServerLogic(deleteProgrammeReq =>
        CoachService.deleteProgramme(deleteProgrammeReq.coachId, deleteProgrammeReq.programme)
      )

  val yaml = OpenAPIDocsInterpreter()
    .serverEndpointsToOpenAPI(
      List(
        getCoachById,
        addCoach,
        deleteCoach,
        getHistoryByAthlete,
        updateHistory,
        getProgrammes,
        addProgramme,
        deleteProgramme
      ),
      "main API",
      "0.1"
    )
    .toYaml

  val swaggerRoutes =
    ZHttp4sServerInterpreter()
      .from(SwaggerUI[RIO[Has[CoachService[Task]] with Clock with Blocking, *]](yaml))
      .toRoutes

  val routes: HttpRoutes[
    ZIO[Has[CoachService[Task]] with Has[Clock.Service] with Has[Blocking.Service], Throwable, *]
  ] =
    ZHttp4sServerInterpreter()
      .from(
        List(
          getCoachById,
          addCoach,
          deleteCoach,
          getHistoryByAthlete,
          updateHistory,
          getProgrammes,
          addProgramme,
          deleteProgramme
        )
      )
      .toRoutes

  val allRoutes = routes <+> swaggerRoutes
}
