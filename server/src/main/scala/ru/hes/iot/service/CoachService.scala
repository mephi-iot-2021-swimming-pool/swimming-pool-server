package ru.hes.iot.service

import ru.hes.iot.db.CoachDao
import ru.hes.iot.domain.Errors.{AthleteNotFound, CoachNotFound}
import ru.hes.iot.domain.{Athlete, Coach, Programme}
import zio.{Has, IO, Task, URLayer, ZIO}

import java.util.UUID


trait CoachService[F[_]] {
  def addCoach(coach: Coach): F[Unit]

  def deleteCoach(coachId: UUID): F[Unit]

  def getCoachById(coachId: UUID): F[Coach]

  def getHistoryByAthlete(coachId: UUID, athleteName: String): F[Map[String, String]]

  def updateHistory(coachId: UUID, athleteName: String, newTrainings: Map[String, String]): F[Unit]

  def getProgrammes(coachId: UUID): F[Seq[Programme]]

  def addProgramme(coachId: UUID, programme: Programme): F[Unit]

  def deleteProgramme(coachId: UUID, programme: Programme): F[Unit]
}

class CoachServiceImpl(
    dao: CoachDao[Task]
) extends CoachService[Task] {

  override def addCoach(coach: Coach): Task[Unit] = dao.insertCoach(coach)

  override def deleteCoach(coachId: UUID): Task[Unit] = dao.deleteCoach(coachId)

  override def getCoachById(coachId: UUID): Task[Coach] =
    dao.getCoach(coachId).someOrFail(CoachNotFound("Couldn't find coach by provided Id"))

  override def getHistoryByAthlete(
      coachId: UUID,
      athleteName: String
  ): Task[Map[String, String]] =
    for {
      coach <- dao.getCoach(coachId).someOrFail(CoachNotFound("Couldn't find coach by provided Id"))
      athlete <- ZIO.getOrFailWith(AthleteNotFound("Couldn't find athlete by provided name"))(
        coach.athletes
          .find(_.name == athleteName)
      )
      history = athlete.history
    } yield history

  override def updateHistory(
      coachId: UUID,
      athleteName: String,
      newTrainings: Map[String, String]
  ): Task[Unit] =
    for {
      coach <- dao.getCoach(coachId).someOrFail(CoachNotFound("Couldn't find coach by provided Id"))
      athlete <- ZIO.getOrFailWith(AthleteNotFound("Couldn't find athlete by provided name"))(
        coach.athletes
          .find(_.name == athleteName)
      )
      historyUpdated = athlete.history ++ newTrainings
      athleteListUpdated = Athlete(athleteName, historyUpdated) +: coach.athletes.filterNot(
        _.name == athleteName
      )
      _ <- dao.updateCoach(coachId, coach.copy(athletes = athleteListUpdated))
    } yield ()

  override def getProgrammes(coachId: UUID): Task[Seq[Programme]] =
    for {
      coach      <- dao.getCoach(coachId).someOrFail(CoachNotFound("Couldn't find coach by provided Id"))
      programmes = coach.programmes
    } yield programmes

  override def addProgramme(coachId: UUID, programme: Programme): Task[Unit] =
    for {
      coach             <- dao.getCoach(coachId).someOrFail(CoachNotFound("Couldn't find coach by provided Id"))
      programmesUpdated = programme +: coach.programmes
      _                 <- dao.updateCoach(coachId, coach.copy(programmes = programmesUpdated))
    } yield ()

  override def deleteProgramme(coachId: UUID, programme: Programme): Task[Unit] =
    for {
      coach             <- dao.getCoach(coachId).someOrFail(CoachNotFound("Couldn't find coach by provided Id"))
      programmesUpdated = coach.programmes.filterNot(_ == programme)
      _                 <- dao.updateCoach(coachId, coach.copy(programmes = programmesUpdated))
    } yield ()
}

object CoachService {
  def getCoachById(coachId: UUID): ZIO[Has[CoachService[Task]], Throwable, Coach] =
    ZIO.serviceWith[CoachService[Task]](_.getCoachById(coachId = coachId))

  def addCoach(coach: Coach): ZIO[Has[CoachService[Task]], Throwable, Unit] =
    ZIO.serviceWith[CoachService[Task]](_.addCoach(coach = coach))

  def deleteCoach(coachId: UUID): ZIO[Has[CoachService[Task]], Throwable, Unit] =
    ZIO.serviceWith[CoachService[Task]](_.deleteCoach(coachId = coachId))

  def getHistoryByAthlete(
      coachId: UUID,
      athleteName: String
  ): ZIO[Has[CoachService[Task]], Throwable, Map[String, String]] =
    ZIO.serviceWith[CoachService[Task]](
      _.getHistoryByAthlete(coachId = coachId, athleteName = athleteName)
    )

  def updateHistory(
      coachId: UUID,
      athleteName: String,
      newTrainings: Map[String, String]
  ): ZIO[Has[CoachService[Task]], Throwable, Unit] = ZIO.serviceWith[CoachService[Task]](
    _.updateHistory(coachId = coachId, athleteName = athleteName, newTrainings = newTrainings)
  )

  def getProgrammes(coachId: UUID): ZIO[Has[CoachService[Task]], Throwable, Seq[Programme]] =
    ZIO.serviceWith[CoachService[Task]](
      _.getProgrammes(coachId = coachId)
    )

  def addProgramme(
      coachId: UUID,
      programme: Programme
  ): ZIO[Has[CoachService[Task]], Throwable, Unit] = ZIO.serviceWith[CoachService[Task]](
    _.addProgramme(coachId = coachId, programme = programme)
  )

  def deleteProgramme(
      coachId: UUID,
      programme: Programme
  ): ZIO[Has[CoachService[Task]], Throwable, Unit] = ZIO.serviceWith[CoachService[Task]](
    _.deleteProgramme(coachId = coachId, programme = programme)
  )

  val live: URLayer[Has[CoachDao[Task]], Has[CoachService[Task]]] =
    (new CoachServiceImpl(_)).toLayer
}
