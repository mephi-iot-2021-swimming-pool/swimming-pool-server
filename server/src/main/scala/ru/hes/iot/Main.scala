package ru.hes.iot

import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.Router
import ru.hes.iot.api.Routes.allRoutes
import ru.hes.iot.db.CoachDao
import ru.hes.iot.db.impl.mongo.config.{MongoConnectionConfig, MongoDaoConfig}
import ru.hes.iot.service.CoachService
import zio.blocking.Blocking
import zio.clock.Clock
import zio.interop.catz._
import zio.magic._
import zio.{App, ExitCode, Has, RIO, Task, URIO, ZEnv, ZIO, ZLayer}

object Main extends App {

  val serve =
    ZIO.runtime[ZEnv with Has[CoachService[Task]]].flatMap { implicit runtime =>
      BlazeServerBuilder[RIO[Has[CoachService[Task]] with Clock with Blocking, *]]
        .bindHttp(8081, "0.0.0.0")
        .withHttpApp(Router("/" -> allRoutes).orNotFound)
        .serve
        .compile
        .drain
    }

  val services = ZLayer.wire[Has[CoachService[Task]]](
    ZLayer.succeed(MongoDaoConfig("RE_IOT_swimming-pool", "MVP")),
    CoachService.live,
    CoachDao.live,
    MongoConnectionConfig.live,
    db.DBConnectionLive.layer
  )

  override def run(args: List[String]): URIO[zio.ZEnv, ExitCode] =
    serve.provideCustomLayer(services).forever.exitCode
}
