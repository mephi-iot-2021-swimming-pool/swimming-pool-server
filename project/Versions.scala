object Versions {
  val tapir = "0.19.3"
  val zio = "1.0.13"
  val mouse = "1.0.6"
  val logback = "1.2.6"
  val zioHttpClient = "3.3.16"
  val mongo = "4.4.0"
  val config = "1.4.1"
  val codecs = "0.12.8"
  val slf4j = "1.7.32"
}