import sbt._

object Dependencies {
  val deps = Dependencies

  val tapir = Seq("com.softwaremill.sttp.tapir" %% "tapir-zio" % Versions.tapir,
    "com.softwaremill.sttp.tapir" %% "tapir-zio-http4s-server" % Versions.tapir)
  val `tapir-circe` = Seq("com.softwaremill.sttp.tapir" %% "tapir-json-circe" % Versions.tapir)
  val `tapir-swagger` = Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % Versions.tapir,
    "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml" % Versions.tapir,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui" % Versions.tapir
  )
  val `tapir-client` = Seq("com.softwaremill.sttp.tapir" %% "tapir-sttp-client" % Versions.tapir)

  val `zio-http-client` = Seq("com.softwaremill.sttp.client3" %% "async-http-client-backend-zio" % Versions.zioHttpClient)

  val zio = Seq(
    "dev.zio" %% "zio" % Versions.zio,
    "io.github.kitlangton" %% "zio-magic" % "0.3.9",
    "dev.zio" %% "zio-config" % "1.0.10",
    "dev.zio" %% "zio-config-magnolia" % "1.0.10",
    "dev.zio" %% "zio-config-typesafe" % "1.0.10",
    "dev.zio" %% "zio-config-refined" % "1.0.10"
  )

  val `zio-test` = Seq(
    "dev.zio" %% "zio-test" % Versions.zio % Test,
    "dev.zio" %% "zio-test-sbt" % Versions.zio % Test,
    "dev.zio" %% "zio-test-magnolia" % Versions.zio % Test
  )

  val mouse = Seq("org.typelevel" %% "mouse" % Versions.mouse)

  val logback = Seq(
    "ch.qos.logback" % "logback-classic" % Versions.logback % Runtime,
    "ch.qos.logback" % "logback-core" % Versions.logback % Runtime
  )

  val slf4j = Seq("org.slf4j" % "slf4j-api" % Versions.slf4j)

  val derevo = Seq("tf.tofu" %% "derevo-circe" % Versions.codecs, "tf.tofu" %% "derevo-pureconfig" % Versions.codecs)

  val config = Seq("com.typesafe" % "config" % Versions.config)

  val mongo = Seq("org.mongodb.scala" %% "mongo-scala-driver" % Versions.mongo)

  val core =
    zio ++
      tapir ++
      `tapir-circe` ++
      `tapir-swagger` ++
      mouse ++
      logback ++
      slf4j ++
      mongo
}